<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>Reciclagem Eletrônica</title>
<?php include "links.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div class="row">   
  <?php include "submenu_home.php"; ?>
  <h1>Soluções </h1>
<p>A poluição atmosférica, das águas e do solo já é responsável por milhares de casos
de doenças e mortes, especialmente entre recém-nascidos. Além disso, a escassez
de água doce potável é uma realidade em vários países do mundo. Existe um
círculo vicioso em que, para suprir as necessidades humanas, promove-se uma
deterioração ainda maior do ambiente, gerando problemas adicionais a longo prazo
e ameaçando a própria existência humana (MOTA, 2006).</p> 
<p>O lixo eletrônico descartado contém diversos componentes que representam riscos
à saúde humana, animal e ao ambiente. Quando esse lixo não é descartado
adequadamente, tais riscos se tornam uma realidade presente na vida de todos.
Infelizmente, o descarte inadequado persiste, e quanto mais tempo perdurar, mais
consequências surgirão e mais difícil será a reparação dos danos.</p>

<p> A necessidade de combater os problemas ambientais não se limita apenas à
promulgação de leis pelo governo; é fundamental mobilizar esforços para encontrar
métodos produtivos mais eficientes que minimizem a emissão de poluentes,
desenvolver formas eficazes de tratamento dos resíduos gerados e, acima de tudo,
promover a conscientização das pessoas, especialmente aquelas envolvidas no
ambiente educacional, para que adotem atitudes e comportamentos voltados para a
sustentabilidade. </p>


</div>
<?php include "footer.php"; ?>
</body>
</html>
