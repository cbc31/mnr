
<div class="container-footer">
    
    <div class="left">
        <div class="titulo-footer">Reciclagem Eletrônica</div>
        <p class="texto-informativo">Gerenciamento do lixo eletrônico - IFNMG-Campus Araçuaí </p>
    </div>
    <div class="center">
        <div class="titulo-footer">Para mais informações acesse:</div>
        
        <ul>
            <li><a href="apoio.php">Como colaborar</a></li>
            <li><a href="termos_de_uso.php">Termos de uso</a></li>
        </ul>
    </div>
    <div class="right">
        <div class="titulo-footer">Saiba mais sobre o nosso projeto em:</div>
        <ul>
            <li><a href="sobre_projeto.php">Fale conosco</a></li>
            <li><a href="equipe.php">Membros do projeto</a></li>
            <li><a href="index.php">Objetivo do projeto</a></li>
        </ul>
    </div>
    <div class="footer">
</div>
<!--nav id="rodape"> 
  <ul> 
    <li><a href="#">Home</a></li>
    <li><a href="#">Produtos</a></li>
    <li><a href="#">Missão</a></li>
    <li><a href="#">Links</a></li>
    <li><a href="#">Contato</a></li>
  </ul> 
</nav-->
<div class="social">
  <!-- Add font awesome icons -->
  </style>
</head>
<body>
    <div class="container">
        <div class="thank-you">Obrigado por visitar nosso site!</div>
        <div class="message"> Fique à vontade para nos seguir nas redes sociais e se inscrever.</div>
        
        <div class="social-media">
            <a href="https://facebook.com" target="_blank">Facebook</a>
            <a href="#" class="fa fa-facebook"></a>
            <a href="https://instagram.com" target="_blank">Instagram</a>
            <a href="#" class="fa fa-instagram"></a>
            <a href="https://youtube.com" target="_blank">Youtube</a>
            <a href="#" class="fa fa-youtube"></a>
        </div>

        <div class="footer">
            &copy; 2024. Todos os direitos reservados.
        </div>
    </div>
</body>
</html>