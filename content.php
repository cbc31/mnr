<div class="col-6 col-s-9">
    <h1>Propósito</h1>
    <p>O Brasil é o quinto maior produtor de lixo eletrônico do mundo e nos últimos anos o descarte incorreto desse lixo vem aumentando em grande percentual; o que acaba gerando dois grandes problemas: os impactos ambientais de grande escala para a natureza; o desperdício de algumas peças que poderiam ser usadas na construção de novos equipamentos eletrônicos.</p>
    <p>Pensando na questão ambiental e econômica, os alunos do 2° ano de informática do IFNMG campus Araçuaí de 2023 sob a orientação do professor Jeancarlo Campos Leão,  desenvolveram um projeto de robótica que visa estudar o reaproveitamento de lixo eletrônico para construção de novos dispositivos inclusive robôs, para auxiliar em questões do nosso dia a dia; além disso o projeto tem como objetivo principal reduzir os impactos que essas peças causam ao meio ambiente e economizar custos de aquisição de novos componentes para o laboratório Kiau Maker.</p>
</div>
